import re
import logging
from json.decoder import JSONDecodeError
from dataclasses import dataclass, asdict, fields, replace
from typing import Dict, Literal, List, Union

import requests

API_KEY = 'AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w'
EMAIL = 'a.abounegm@innopolis.university'
BASE_URL = f'https://script.google.com/macros/s/{API_KEY}/exec'
SPEC_URL = BASE_URL + f'?service=getSpec&email={EMAIL}'
PRICE_API_URL = BASE_URL + f'?service=calculatePrice&email={EMAIL}'

logging.basicConfig()

@dataclass
class Spec:
    budget_price_per_min: int
    luxury_price_per_min: int
    fixed_price_per_km: int
    allowed_deviation: int
    inno_discount: int

spec_pattern = re.compile(r'''Here is InnoCar Specs:
Budet car price per minute = (?P<budget_price_per_min>\d+)
Luxury car price per minute = (?P<luxury_price_per_min>\d+)
Fixed price per km = (?P<fixed_price_per_km>\d+)
Allowed deviations in % = (?P<allowed_deviation>\d+)
Inno discount in % = (?P<inno_discount>\d+)
''')

response = requests.get(SPEC_URL)
matches = spec_pattern.search(response.text).groupdict()
spec_dict = { field.name: field.type(matches[field.name]) for field in fields(Spec) }
spec = Spec(**spec_dict)
print('Spec:', spec)


@dataclass
class TestCase:
    type: Literal['budget', 'luxury', 'nonsense']
    plan: Literal['minute', 'fixed_price', 'nonsense']
    distance: int
    planned_distance: int
    time: int
    planned_time: int
    inno_discount: Literal['yes', 'no', 'nonsense']
    @property
    def is_valid(self: 'TestCase') -> bool:
        if self.type not in ['budget', 'luxury']:
            return False
        if self.plan not in ['minute', 'fixed_price']:
            return False
        if self.plan == 'fixed_price' and self.type != 'budget':
            return False
        if self.distance <= 0:
            return False
        if self.planned_distance <= 0:
            return False
        if self.time <= 0:
            return False
        if self.planned_time <= 0:
            return False
        if self.inno_discount not in ['yes', 'no']:
            return False
        return True


def expected_price(test_case: TestCase) -> Union[Literal['Invalid Request'], int]:
    if not test_case.is_valid:
        return 'Invalid Request'
    if test_case.plan == 'fixed_price':
        deviation_dist = (test_case.planned_distance - test_case.distance) / test_case.planned_distance * 100
        deviation_time = (test_case.planned_time - test_case.time) / test_case.planned_time * 100
        if deviation_dist > spec.allowed_deviation or deviation_time > spec.allowed_deviation:
            test_case = replace(test_case, plan='minute')
        else:
            price = test_case.planned_distance * spec.fixed_price_per_km
    if test_case.plan == 'minute':
        if test_case.type == 'budget':
            price = test_case.time * spec.budget_price_per_min
        elif test_case.type == 'luxury':
            price = test_case.time * spec.luxury_price_per_min

    if test_case.inno_discount == 'yes':
        return price * (1 - spec.inno_discount / 100)
    elif test_case.inno_discount == 'no':
        return price

'''
test_cases: List[TestCase] = [
    # Budget type
    ## Distance
    ### Positive
    TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes'),
    ### Zero
    TestCase(type='budget', plan='minute', distance=0, planned_distance=1, time=1, planned_time=1, inno_discount='yes'),
    ### Negative
    TestCase(type='budget', plan='minute', distance=-1, planned_distance=1, time=1, planned_time=1, inno_discount='yes'),
    ## Planned distance
    ### Positive
    TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes'),
    ### Zero
    TestCase(type='budget', plan='minute', distance=1, planned_distance=0, time=1, planned_time=1, inno_discount='yes'),
    ### Negative
    TestCase(type='budget', plan='minute', distance=1, planned_distance=-1, time=1, planned_time=1, inno_discount='yes'),
    ## Time
    ### Positive
    TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes'),
    ### Zero
    TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=0, planned_time=1, inno_discount='yes'),
    ### Negative
    TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=-1, planned_time=1, inno_discount='yes'),
    ## Planned time
    ### Positive
    TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes'),
    ### Zero
    TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=1, planned_time=0, inno_discount='yes'),
    ### Negative
    TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=1, planned_time=-1, inno_discount='yes'),
    ## Valid
    TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes'),
    # Nonsense type
    TestCase(type='nonsense', plan='minute', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes'),
    # Nonsense plan
    TestCase(type='budget', plan='nonsense', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes'),
    # Nonsense inno_discount
    TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='nonsense'),

    # Fixed plans
    TestCase(type='budget', plan='fixed_price', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes'),
]
'''

test_cases: List[TestCase] = [
    # Fail
    TestCase(distance=-1, planned_distance=1,  time=1,  planned_time=1,  type='budget',   plan='minute',      inno_discount='yes'),
    TestCase(distance=1,  planned_distance=-1, time=1,  planned_time=1,  type='budget',   plan='minute',      inno_discount='yes'),
    TestCase(distance=1,  planned_distance=1,  time=-1, planned_time=1,  type='budget',   plan='minute',      inno_discount='yes'),
    TestCase(distance=1,  planned_distance=1,  time=1,  planned_time=-1, type='budget',   plan='minute',      inno_discount='yes'),
    TestCase(distance=1,  planned_distance=1,  time=1,  planned_time=1,  type='nonsense', plan='minute',      inno_discount='yes'),
    TestCase(distance=1,  planned_distance=1,  time=1,  planned_time=1,  type='budget',   plan='nonsense',    inno_discount='yes'),
    TestCase(distance=1,  planned_distance=1,  time=1,  planned_time=1,  type='budget',   plan='minute',      inno_discount='nonsense'),
    TestCase(distance=1,  planned_distance=1,  time=1,  planned_time=1,  type='luxury',   plan='fixed_price', inno_discount='yes'),
    # Succeed
    TestCase(distance=1,  planned_distance=1,  time=1,  planned_time=1,  type='budget',   plan='minute',      inno_discount='no'),
    TestCase(distance=1,  planned_distance=1,  time=1,  planned_time=1,  type='budget',   plan='minute',      inno_discount='yes'),
    TestCase(distance=1,  planned_distance=1,  time=1,  planned_time=1,  type='budget',   plan='fixed_price', inno_discount='no'),
    TestCase(distance=1,  planned_distance=1,  time=1,  planned_time=1,  type='budget',   plan='fixed_price', inno_discount='yes'),
    TestCase(distance=1,  planned_distance=1,  time=1,  planned_time=1,  type='luxury',   plan='minute',      inno_discount='no'),
    TestCase(distance=1,  planned_distance=1,  time=1,  planned_time=1,  type='luxury',   plan='minute',      inno_discount='yes'),
]


failed = 0
for idx, test_case in enumerate(test_cases, 1):
    print(f'Running test case {idx} / {len(test_cases)}', end='\r')
    data = asdict(test_case)
    response = requests.get(PRICE_API_URL, params=data)

    expected_result = expected_price(test_case)

    try:
        result = response.json()['price']
    except JSONDecodeError:
        result = response.text

    if result != expected_result:
        logging.error(f'Expected "{expected_result}" but got "{result}" for {test_case}')
        failed += 1

print()
print(f'Ran {len(test_cases)} tests. {failed} tests failed.')
